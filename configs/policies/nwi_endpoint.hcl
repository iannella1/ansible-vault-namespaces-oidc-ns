# This policy document controls what exactly a given role with said policy can perform
# This policy is written with a PKI engine that is named `inter_ca`

path "inter_ca*" { 
    capabilities = ["read", "list"] 
}
path "inter_ca/roles/nwi_endpoint" { 
    capabilities = ["create", "update"] 
}
path "inter_ca/sign/nwi_endpoint" { 
    capabilities = ["create", "update"] 
}
path "inter_ca/issue/nwi_endpoint" { 
    capabilities = ["create"] 
}